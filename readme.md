# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EKATERINA GERASIMOVA

**E-MAIL**: kogryster@gmail.com

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOT

https://yadi.sk/d/-Xg1JJV1FHbg7w?w=1
